package com.mysourcecode.dev.findlostitems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindlostitemsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindlostitemsApplication.class, args);
	}

}
